{
  seed: 543210,
  
  structure: [ "/usr/share/marley/structure/z019",
               "/usr/share/marley/structure/z018",
               "/usr/share/marley/structure/z017", ],

  reactions: [ "/usr/share/marley/react/ve40ArCC_1998.react" ],

  source: {
   type: "fermi-dirac",
   neutrino: "ve",
   Emin: 0,
   Emax: 60,
   temperature: 3.5,
   eta: 0
  },

  log: [ //{ file: "log", level: "debug", overwrite: true },
         { file: "stdout", level: "info" },
       ],
}
