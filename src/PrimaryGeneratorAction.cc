// standard library includes
#include <iostream>

// Geant4 includes
#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4PhysicalConstants.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4SystemOfUnits.hh"

// MARLEY includes
#include "marley/Event.hh"
#include "marley/JSON.hh"
#include "marley/Logger.hh"
#include "marley/RootJSONConfig.hh"

#include "PrimaryGeneratorAction.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction(
  const std::string& config_file_name) : G4VUserPrimaryGeneratorAction(),
  marley_generator_(nullptr)
{
  // Initialize the MARLEY logger
  marley::Logger::Instance().add_stream(std::cout,
    marley::Logger::LogLevel::INFO);

  // Create a new generator object using the configuration stored in
  // the given file
  marley::JSON json = marley::JSON::load_file(config_file_name);
  marley::RootJSONConfig config(json);

  marley_generator_= std::make_unique<marley::Generator>(
    config.create_generator() );
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4PrimaryVertex* vertex = new G4PrimaryVertex(0., 0., 0., 0.); // x,y,z,t0

  marley::Event ev = marley_generator_->create_event();

  std::cout << ev << '\n';

  for ( const auto& fp : ev.get_final_particles() ) {

    G4PrimaryParticle* particle = new G4PrimaryParticle( fp->pdg_code(),
      fp->px(), fp->py(), fp->pz(), fp->total_energy() );

    particle->SetCharge( fp->charge() );

    vertex->SetPrimary(particle);
  }

  anEvent->AddPrimaryVertex(vertex);
}
