// Geant4 includes
#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4SubtractionSolid.hh"
#include "G4PVPlacement.hh"

// My includes
#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction()
{
}

G4VPhysicalVolume* DetectorConstruction::Construct() {

  //auto* material_manager = G4NistManager::Instance();
  //auto* air = material_manager->FindOrBuildMaterial("G4_AIR");

  G4Box* box = new G4Box("box", 100.*cm, 100.*cm, 100.*cm);

  G4Material* liquid_argon = new G4Material("liquidArgon", 18, 39.95*g/mole,
    1.390*g/cm3);

  G4LogicalVolume* logical_world = new G4LogicalVolume( box,
    liquid_argon, liquid_argon->GetName() );

  /*[[maybe_unused]]*/ G4VPhysicalVolume* my_box = new G4PVPlacement(
    0,  // no rotation
    G4ThreeVector(),  // at (0, 0, 0)
    logical_world, // its logical volume
    "MyBox",       // its name
    0,             // its mother logical volume
    false,         // no boolean operation
    0);            // copy number

  world_ = new G4PVPlacement(0, G4ThreeVector(), logical_world, "WorldBox",
    nullptr, false, 0);

  return world_;
}

const G4VPhysicalVolume* DetectorConstruction::GetWorld() const {
  return world_;
}
