#include <memory>

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "G4PhysListFactory.hh"
#include "G4VModularPhysicsList.hh"

#include "DetectorConstruction.hh"
//#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
//#include "RunAction.hh"
//#include "StackingAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "TrackingAction.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

int main(int argc, char* argv[]) {

  if (argc <= 1) {
    std::cout << "Usage: example MARLEY_CONFIG_FILE\n";
    return 1;
  }

  std::string config_file_name( argv[1] );

  std::unique_ptr<G4RunManager> rm(new G4RunManager);

  // set mandatory initialization classes
  DetectorConstruction* det = new DetectorConstruction();
  rm->SetUserInitialization(det);

  //PhysicsList* phys = new PhysicsList;
  //rm->SetUserInitialization(phys);
  G4PhysListFactory factory;
  G4VModularPhysicsList* refList = factory.GetReferencePhysList("QGSP_BIC_HP");
  rm->SetUserInitialization(refList);

  TrackingAction* ta = new TrackingAction;
  rm->SetUserAction(ta);

  SteppingAction* sa = new SteppingAction();
  rm->SetUserAction(sa);

  PrimaryGeneratorAction* pga = new PrimaryGeneratorAction(config_file_name);
  rm->SetUserAction(pga);

  EventAction* eva = new EventAction;
  rm->SetUserAction(eva);

  //StackingAction* stacka = new StackingAction;
  //rm->SetUserAction(stacka);

  //RunAction* ra = new RunAction;
  //rm->SetUserAction(ra);

  rm->Initialize();

  // save geometry in GDML format
  //G4GDMLParser gdml_parser;
  //gdml_parser.Write("world.gdml", det->GetWorld());

  size_t num_events = 100;

  std::cout << "Simulating " << num_events << " events . . .\n";

  rm->BeamOn(num_events);

  return 0;
}
